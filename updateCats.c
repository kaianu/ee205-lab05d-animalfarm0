///////////////////////////////////////////////////////////////////////////////
///         University of Hawaii, College of Engineering
/// @brief  Lab 05d - Animal Farm 0 - EE 205 - Spr 2022
///
///
/// @file updateCats.c
/// @version 1.0
///
/// function: add cats to database
///
/// @author Kaianu Reyes-Huynh <kaianu@hawaii.edu>
/// @date   02/17/2022
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "updateCats.h"
#include "catDatabase.h"

void updateCatName(int index, char newName[MAX_LENGTH]){

   strcpy(catName[index], newName);
}

void fixCat(int index){

   is_fixed[index] = 1;
}

void updateCatWeight(int index, float newWeight){
   
   if (newWeight < 0){
      printf("Error: Weight cannot be less than 0.\n");
      exit(EXIT_FAILURE);
   }
   else{
      weight[index] = newWeight;
   }
}

