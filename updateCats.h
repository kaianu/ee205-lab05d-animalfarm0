///////////////////////////////////////////////////////////////////////////////
///         University of Hawaii, College of Engineering
/// @brief  Lab 05d - Animal Farm 0 - EE 205 - Spr 2022
///
///
/// @file updateCats.h
/// @version 1.0
///
/// function: add cats to database
///
/// @author Kaianu Reyes-Huynh <kaianu@hawaii.edu>
/// @date   02/17/2022
///////////////////////////////////////////////////////////////////////////////

#include "catDatabase.h"
#pragma once

void updateCatName(int index, char newName[MAX_LENGTH]);

void fixCat(int index);

void updateCatWeight(int index, float newWeight);
