///////////////////////////////////////////////////////////////////////////////
///         University of Hawaii, College of Engineering
/// @brief  Lab 05d - Animal Farm 0 - EE 205 - Spr 2022
///
///
/// @file reportCats.c
/// @version 1.0
///
/// function: add cats to database
///
/// @author Kaianu Reyes-Huynh <kaianu@hawaii.edu>
/// @date   02/17/2022
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include "addCat.h"
#include "catDatabase.h"
#include "reportCats.h"

//print cat given the spot in the database
void printCat(int index){

   if (index < 0 || index > numCats){
      printf("animalFarm0: Bad Cat [%d]\n", index);
      exit(EXIT_FAILURE);
   }

   printf("cat index = [%d]\tname = [%c]\tgender = [%d]\tbreed = [%d]\tisFixed = [%d]\tweight = [%f]\n", index, catName[index][MAX_LENGTH], gender[index], breed[index], is_fixed[index], weight[index]);
}

//print all cats
void printAllCats(){
   
   //go through all of the index while it not going over the ammount stored and print all of the specifications
   for (int index = 0; index <= numCats; index++){
   
      printf("cat index = [%d]\tname =[%c]\tgender = [%d]\tbreed = [%d]\tisFixed = [%d]\tweight = [%f]\n", index, catName[index][MAX_LENGTH], gender[index], breed[index], is_fixed[index], weight[index]);

   }
}

int findCat(char name[MAX_LENGTH]){
   
   int temp;
   //go through all of the cat names and see if there is a match
   for(int index = 0; index <= numCats; index++){
      
      if (name == catName[index]){
         temp = index;
         index = numCats + 1;
      }  
   }

   return temp;
}

