///////////////////////////////////////////////////////////////////////////////
///         University of Hawaii, College of Engineering
/// @brief  Lab 05d - Animal Farm 0 - EE 205 - Spr 2022
///
///
/// @file addCat.c
/// @version 1.0
///
/// function: add cats to database
///
/// @author Kaianu Reyes-Huynh <kaianu@hawaii.edu>
/// @date   02/17/2022
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include "addCat.h"
#include "catDatabase.h"

int numCats = 0;

int addCat( char CatName[MAX_LENGTH], enum catGender Gender, enum catBreed Breed, bool IsFixed, float Weight ) {

   size_t nameLength = strlen(CatName);

   //if the number of cats is greater than the database can hold, exit function
   if (numCats > MAX_CATS){
      printf("Error: Too many cats.\n");
      exit(EXIT_FAILURE);
   }
   //if number of characters in name is empty, exit function
   if ( nameLength == 0 ){
      printf("Error: Cat name cannot be empty.\n");
      exit(EXIT_FAILURE);
   }
   //if number of characters in array is greater than 30, exit function
   if ( nameLength > MAX_LENGTH ){
      printf("Error: Cat name cannot be greater than 30 characters.\n");
      exit(EXIT_FAILURE);
   }
  
   //check if the character array is unique
   for (int i = 1; i != numCats; i++){
      if (CatName[numCats] == CatName[numCats - i]){
         printf("Error: Cat name has to be unique.\n");
         exit(EXIT_FAILURE);
      }
   }
   //weight cannot be less than 0
   if (Weight < 0){
      printf("Error: Cat weight cannot be less than 0.\n");
      exit(EXIT_FAILURE);
   }
   //store all data into arrays
   strncpy(catName[numCats], CatName, MAX_LENGTH);
   gender[numCats] = Gender;
   breed[numCats] = Breed;
   is_fixed[numCats] = IsFixed;
   weight[numCats] = Weight;

   numCats = numCats + 1;

   return numCats;
}
