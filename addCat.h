///////////////////////////////////////////////////////////////////////////////
///         University of Hawaii, College of Engineering
/// @brief  Lab 05d - Animal Farm 0 - EE 205 - Spr 2022
///
///
/// @file addCat.h
/// @version 1.0
///
/// @author Kaianu Reyes-Huynh <kaianu@hawaii.edu>
/// @date   02/17/2022
///////////////////////////////////////////////////////////////////////////////

#include <stdbool.h>
#include "catDatabase.h"
#pragma once


//headerfile for adding cats
extern int addCat(  char CatName[MAX_LENGTH], enum catGender Gender, enum catBreed Breed, bool IsFixed, float Weight );
