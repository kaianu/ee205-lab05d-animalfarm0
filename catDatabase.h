///////////////////////////////////////////////////////////////////////////////
///         University of Hawaii, College of Engineering
/// @brief  Lab 05d - Animal Farm 0 - EE 205 - Spr 2022
///
///
/// @file catDatabase.h
/// @version 1.0
///
/// @author Kaianu Reyes-Huynh <kaianu@hawaii.edu>
/// @date   02/17/2022
///////////////////////////////////////////////////////////////////////////////

#include <stdbool.h>
#include <string.h>

#define MAX_LENGTH  31    //Max length of characters in a cat's name
#define MAX_CATS    29    //Max number of cats in the array

#pragma once

enum catGender { UNKNOWN_GENDER, MALE, FEMALE }; 
enum catBreed  { UNKNOWN_BREED, MAINE_COON, MANX, SHORTHAIR, PERSIAN, SPHYNX };


//declaring cat description arrays
extern char  catName[MAX_LENGTH][MAX_CATS];
extern enum  catGender gender[MAX_CATS];
extern enum  catBreed breed[MAX_CATS];
extern bool  is_fixed[MAX_CATS];
extern float weight[MAX_CATS];

//intialize arrays
//memset(catName, "0", sizeof catName );
//memset(gender, 0, sizeof gender );
//memset(breed, 0, sizeof breed );
//memset(is_fixed, 0, sizeof is_fixed );
//memset(weight, 0, sizeof weight );

//global variable to indicated the current number of cats
extern int numCats;

//initialize initializeDatabase()
extern void initializeDatabase();

