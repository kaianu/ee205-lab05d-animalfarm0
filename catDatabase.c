///////////////////////////////////////////////////////////////////////////////
///         University of Hawaii, College of Engineering
/// @brief  Lab 05d - Animal Farm 0 - EE 205 - Spr 2022
///
///
/// @file catDatabase.c
/// @version 1.0
///
/// @author Kaianu Reyes-Huynh <kaianu@hawaii.edu>
/// @date   02/17/2022
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include "catDatabase.h"

void initializeDatabase(){

   //catName initialization
   for(int j = 0; j < MAX_LENGTH; j++){
        
      for(int i = 0; i < MAX_CATS; i++){           
         catName[i][j] = '0';   
      }
   }

   //gender initialization
   for( int i = 0; i < MAX_CATS; i++){
      gender[i] = 0;
   }

   //breed initialization
   for( int i = 0; i < MAX_CATS; i++){
      breed[i] = 0;
   }

   //is_fixed initialization
   for( int i = 0; i < MAX_CATS; i++){
      is_fixed[i] = 0;
   }

   //weight initialization
   for( int i = 0; i < MAX_CATS; i++){
      weight[i] = 0.0;
   }
}



